﻿using System.ComponentModel;
using System.ServiceProcess;

namespace NcpWatchDogService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
        
        protected override void OnCommitted(System.Collections.IDictionary savedState)
        { 
            new ServiceController(serviceInstaller.ServiceName).Start();
        }
    }
}