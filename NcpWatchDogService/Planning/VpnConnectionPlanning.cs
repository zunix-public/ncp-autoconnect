﻿using System;
using System.Linq;
using Common;

namespace NcpWatchDogService.Planning
{
    public class VpnConnectionPlanning
    {
        readonly DayOfWeek[] _daysOfWeek;
        Daytime _connectionTime;
        Daytime _deconnectionTime;
        public VpnConnectionPlanning(DayOfWeek[] daysOfWeek, Daytime connectionTime, Daytime deconnectionTime)
        {
            _daysOfWeek = daysOfWeek;
            _connectionTime = connectionTime;
            _deconnectionTime = deconnectionTime;
        }

        public bool CanConnectNow()
        {
            return CanConnectToday() && CanConnectAtCurrentDayTime();
        }

        private bool CanConnectToday()
        {
            var dayOfWeek = DateTime.Now.DayOfWeek;
            return _daysOfWeek.Any(d => d == dayOfWeek);
        }

        private bool CanConnectAtCurrentDayTime()
        {
            var dayTime = Daytime.Now;
            return dayTime.TotatSeconds >= _connectionTime.TotatSeconds
                && dayTime.TotatSeconds <= _deconnectionTime.TotatSeconds;
        }
    }
}