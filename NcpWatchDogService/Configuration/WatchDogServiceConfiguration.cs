﻿using System;using Common;
using Common.Configuration;


namespace NcpWatchDogService.Configuration
{
    public class WatchDogServiceConfiguration : ServiceConfiguration
    {
        public string NcpClientPath { get; set; }
        public double ConnectionCheckIntervalMs { get; set; }
        public Daytime ConnectionDayTime { get; set; }
        public Daytime DeconnectionDayTime { get; set; }
        public DayOfWeek[] ConnectionDays { get; set; }

        public WatchDogServiceConfiguration()
        {
            NcpClientPath = @"C:\Program Files\NCP\SecureClient\ncpclientcmd.exe";
            ConnectionCheckIntervalMs = TimeSpan.FromSeconds(30).TotalMilliseconds;
            ConnectionDayTime = new Daytime(8, 30, 0);
            DeconnectionDayTime = new Daytime(18, 45, 0);
            ConnectionDays = new[] {
                DayOfWeek.Monday,
                DayOfWeek.Tuesday,
                DayOfWeek.Wednesday,
                DayOfWeek.Thursday,
                DayOfWeek.Friday
            };
        }
    }
}
