﻿using Newtonsoft.Json;
using System;
using System.ServiceProcess;
using System.Timers;
using Common.Configuration;
using Common.Logs;
using NcpWatchDogService.Configuration;
using NcpWatchDogService.NcpClient;
using NcpWatchDogService.Planning;

namespace NcpWatchDogService
{
    /// <summary>
    /// Service de démarrage, d'arrêt et de reconnexion authomatique du client vpn NCP
    /// </summary>
    public partial class NcpWatchDogService : ServiceBase
    {
        private const string ConfigurationFileName = "NcpWatchDogServiceConfiguration.xml";

        Timer _vnpAvailabilityTimer;
        ApplicationEventLog _eventLog;
        WatchDogServiceConfiguration _svcConfig; 

        readonly NcpClient.NcpClient _ncpClient;
        readonly VpnConnectionPlanning _vpnConnectionPlanning;
        readonly NcpNetworkAdapterAvalability _ncpNetworkAvalability;
        
     
        bool _isFirstExecution = true;

        public NcpWatchDogService()
        {
            InitializeComponent();
            InitializeLogs();
            LoadConfigurationFileOrDefaultIfError();
            InitializeVpnAvailabilityTimer();
            _ncpClient = new NcpClient.NcpClient(_svcConfig.NcpClientPath);
            _vpnConnectionPlanning = new VpnConnectionPlanning(_svcConfig.ConnectionDays, _svcConfig.ConnectionDayTime, _svcConfig.DeconnectionDayTime);
            _ncpNetworkAvalability = new NcpNetworkAdapterAvalability();
        }

        private void InitializeLogs()
        {
            _eventLog = new ApplicationEventLog(this);
            _eventLog.Init();
        }

        private void LoadConfigurationFileOrDefaultIfError()
        {
            try
            {
                _eventLog.Info("Config file : " + ServiceConfiguration.GetConfigFile(ConfigurationFileName).FullName);
                _svcConfig = ServiceConfiguration.LoadFromFile(ConfigurationFileName, new WatchDogServiceConfiguration());
            }
            catch (Exception ex)
            {
                _eventLog.Error(ex.Message);
                _eventLog.Info("Failed to load config from file. Loading default config.");
                _svcConfig = new WatchDogServiceConfiguration();
            }
        }

        private void InitializeVpnAvailabilityTimer()
        {
            _vnpAvailabilityTimer = new Timer();
            _vnpAvailabilityTimer.Elapsed += OnTimerElapsedTime;
            _vnpAvailabilityTimer.Interval = _svcConfig.ConnectionCheckIntervalMs;
        }

        protected override void OnStart(string[] args)
        {
            _vnpAvailabilityTimer.Start();
            _eventLog.Info("Service is started");
        }

        private void OnTimerElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                if (_isFirstExecution)
                {
                    _eventLog.Info("Config used : " + JsonConvert.SerializeObject(_svcConfig));
                }

                if (IsConnectionNeeded())
                {
                    _eventLog.WriteEntry("VPN reconnection needed");
                    TryConnect();
                }
                else if (IsDisconnectionNeeded())
                {
                    _eventLog.WriteEntry("VPN deconnection needed");
                    TryDisconnect();
                } 
                else if (_isFirstExecution)
                {
                    _eventLog.Info("Nothing to do");
                }
            }
            catch (Exception ex)
            {
                _eventLog.Error(ex.Message);
            } 
            finally
            {
                _isFirstExecution = false;
            }
        }

        private bool IsConnectionNeeded()
        {
            var canConnectNow = _vpnConnectionPlanning.CanConnectNow();
            var isVpnDisconnected = _ncpNetworkAvalability.IsNcpConnexionDown();
            return isVpnDisconnected && canConnectNow;
        }

        private void TryConnect()
        {
            try
            {
                var reconnectResult = _ncpClient.Connect();
                _eventLog.Info(reconnectResult);
            }
            catch (Exception ex)
            {
                _eventLog.Error(ex.Message);
            }
        }

        private bool IsDisconnectionNeeded()
        {
            var canConnectNow = _vpnConnectionPlanning.CanConnectNow();
            var isVpnConnected = _ncpNetworkAvalability.IsNcpConnexionUp();
            return isVpnConnected && !canConnectNow;
        }

        private void TryDisconnect()
        {
            try
            {
                var disconnectResult = _ncpClient.Disconnect();
                _eventLog.Info(disconnectResult);
            }
            catch(Exception ex)
            {
                _eventLog.Error(ex.Message);
            }
        }

        protected override void OnStop()
        {
            _vnpAvailabilityTimer.Stop();
            _eventLog.Info("Service is stopped");
        }
    }
}