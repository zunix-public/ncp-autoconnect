﻿using System;

namespace NcpWatchDogService.NcpClient
{
    public class NcpClientBusyException : Exception
    {
        public NcpClientBusyException() : base("Ncp client busy") { }
    }
}