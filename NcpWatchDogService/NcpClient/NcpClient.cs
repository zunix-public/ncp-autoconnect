﻿using System.Diagnostics;
using System.IO;

namespace NcpWatchDogService.NcpClient
{
    public class NcpClient
    {
        const string NcpConnectionCommand = "/connect";
        const string NcpDeconnectionCommand = "/disconnect";

        private readonly string _ncpclientcmdPath;
        private bool _isNcpClientBusy;

        public NcpClient(string ncpclientcmd)
        {
            _ncpclientcmdPath = ncpclientcmd;
            _isNcpClientBusy = false;
        }

        public string Connect()
        {
            return NcpClientCommand(NcpConnectionCommand);
        }

        public string Disconnect()
        {
            return NcpClientCommand(NcpDeconnectionCommand);
        }

        private string NcpClientCommand(string argument)
        {
            try
            {
                if (_isNcpClientBusy)
                    throw new NcpClientBusyException();

                if (!File.Exists(_ncpclientcmdPath))
                    throw new NcpClientNotFoundException(_ncpclientcmdPath);

                _isNcpClientBusy = true;
                Process process = new Process
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        WindowStyle = ProcessWindowStyle.Normal,
                        FileName = _ncpclientcmdPath,
                        Arguments = argument,
                        RedirectStandardError = true,
                        RedirectStandardOutput = true
                    }
                };
                process.Start();
                var output = process.StandardOutput.ReadToEnd();
                process.WaitForExit();
                return output;
            }
            finally
            {
                _isNcpClientBusy = false;
            }
        }
    }
}