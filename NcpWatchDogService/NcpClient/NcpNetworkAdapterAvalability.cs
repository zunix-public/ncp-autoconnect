﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using Common.Network;

namespace NcpWatchDogService.NcpClient
{
    public class NcpNetworkAdapterAvalability
    {
        private const string NcpNetworkAdapterDescription = "NCP Secure Client";

        public bool IsNcpConnexionDown()
        {
            return !IsNcpConnexionUp();
        }

        public bool IsNcpConnexionUp()
        {
            var networkAdapterHelper = new NetworkAdapterHelper();
            var ncpAdapter = networkAdapterHelper.GetNcpNetworkAdapterFromDescription(NcpNetworkAdapterDescription);
            return ncpAdapter.OperationalStatus == OperationalStatus.Up;
        }
    }
}