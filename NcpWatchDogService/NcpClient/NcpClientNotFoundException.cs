﻿using System;

namespace NcpWatchDogService.NcpClient
{
    public class NcpClientNotFoundException : Exception
    {
        public NcpClientNotFoundException(string ncpClientPath) : base("Ncp client not found at " + ncpClientPath) { }
    }
}