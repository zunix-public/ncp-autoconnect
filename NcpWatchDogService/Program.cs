﻿using System.ServiceProcess;
using Common.Configuration;
using NcpWatchDogService.Configuration;

namespace NcpWatchDogService
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée de l'application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            var svcConfig = ServiceConfiguration.LoadFromFile("toto", new WatchDogServiceConfiguration());
            ServicesToRun = new ServiceBase[]
            {
                new NcpWatchDogService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}