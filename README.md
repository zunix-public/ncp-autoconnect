# NCP Watchdog
* `Ncp.WatchDog` : Service Windows de connexion, déconnexion et reprise de connexion automatique pour le client VPN NCP.

Projet : 

`./NcpWatchDogService`

## Installation

```bash
./bin/NcpWatchDogServiceSetup.msi
```

## Ncp.WatchDog : règles implémentées
Par défaut :
* Jours : semaine sauf samedi/dimanche
* Heures : de 8H30 à 18H45

Fichier de conf :
> `C:\Program Files\Zunix\NcpWatchDogService\NcpWatchDogServiceConfiguration.xml`
```xml
<?xml version="1.0"?>
<ServiceConfiguration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <NcpClientPath>C:\Program Files\NCP\SecureClient\ncpclientcmd.exe</NcpClientPath>
  <ConnectionCheckIntervalMs>30000</ConnectionCheckIntervalMs>
  <ConnectionDayTime>
    <Hour>8</Hour>
    <Minute>45</Minute>
    <Second>0</Second>
  </ConnectionDayTime>
  <DeconnectionDayTime>
    <Hour>18</Hour>
    <Minute>45</Minute>
    <Second>0</Second>
  </DeconnectionDayTime>
  <ConnectionDays>
    <DayOfWeek>Monday</DayOfWeek>
    <DayOfWeek>Tuesday</DayOfWeek>
    <DayOfWeek>Wednesday</DayOfWeek>
    <DayOfWeek>Thursday</DayOfWeek>
    <DayOfWeek>Friday</DayOfWeek>
  </ConnectionDays>
</ServiceConfiguration>
```