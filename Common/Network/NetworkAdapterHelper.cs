﻿using System;
using System.Linq;
using System.Net.NetworkInformation;

namespace Common.Network
{
    public class NetworkAdapterHelper
    {
        public NetworkInterface GetNcpNetworkAdapterFromDescription(string description)
        {
            var adapters = NetworkInterface.GetAllNetworkInterfaces();
            var ncpAdapter = adapters.FirstOrDefault(adapter => adapter.Description.Contains(description));
            if (ncpAdapter == null)
                throw new Exception("Aucune carte réseau NCP trouvée");

            return ncpAdapter;
        }

        public NetworkInterface GetNcpNetworkAdapterFromName(string name)
        {
            var adapters = NetworkInterface.GetAllNetworkInterfaces();
            var ncpAdapter = adapters.FirstOrDefault(adapter => adapter.Name.Contains(name));
            if (ncpAdapter == null)
                throw new Exception("Aucune carte réseau NCP trouvée");

            return ncpAdapter;
        }

        public string GetIpForAdapter(NetworkInterface networkInterface)
        {
            foreach (UnicastIPAddressInformation ip in networkInterface.GetIPProperties().UnicastAddresses)
            {
                if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    return ip.Address.ToString();
                }
            }
            throw new Exception("IP non trouvée");
        }
    }
}
