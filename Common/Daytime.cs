﻿using System;
using System.Xml.Serialization;

namespace Common
{
    /// <summary>
    /// Modélisation de l'heure de la journée
    /// </summary>
    [Serializable]
    public class Daytime
    {
        [XmlIgnore]
        public static Daytime Now
        {
            get
            {
                var dateTime = DateTime.Now;
                return new Daytime(dateTime.Hour, dateTime.Minute, dateTime.Second);
            }
        }

        [XmlIgnore]
        public int TotatSeconds 
        { 
            get 
            {
                return Hour * 3600 + Minute * 60 + Second;
            }
        }

        public int Hour { get; set; }
        public int Minute { get; set; }
        public int Second { get; set; }

        public Daytime() { }

        public Daytime(int hour, int minute, int second)
        {
            Hour = hour;
            Minute = minute;
            Second = second;
        }
    }
}