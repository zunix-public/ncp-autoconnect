﻿using System;
using System.Diagnostics;
using System.ServiceProcess;

namespace Common.Logs
{
    /// <summary>
    /// Logger pour EventViewer, section Applications
    /// </summary>
    public class ApplicationEventLog : EventLog
    {
        const string Logname = "Application";

        public ApplicationEventLog(ServiceBase service) : base(Logname)
        {
            Source = service.ServiceName;
        }

        public void Init()
        {
            if (!SourceExists(Source))
                CreateEventSource(Source, Logname);
        }

        public void Info(string message)
        {
            WriteEntry($"{DateTime.Now} - {message}", EventLogEntryType.Information);
        }

        public void Error(string message)
        {
            WriteEntry($"{DateTime.Now} - {message}", EventLogEntryType.Error);
        }
    }
}