﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Common.Configuration
{
	[Serializable]
	public class ServiceConfiguration
	{

        public ServiceConfiguration()
		{
        }

		public static T LoadFromFile<T>(string filename, T defaultValue) where T : ServiceConfiguration
        {
            var confFile = GetConfigFile(filename);
			if (!confFile.Exists)
			{
                defaultValue.Save<T>(confFile);
			}

			var serializer = new XmlSerializer(typeof(T));
			using (var fStream = new FileStream(confFile.FullName, FileMode.Open))
				return (T)serializer.Deserialize(fStream);
		} 
		
		private void Save<T>(FileInfo confFile)
		{
			var serializer = new XmlSerializer(typeof(T));
			using (var fStream = new FileStream(confFile.FullName, FileMode.Create))  
				serializer.Serialize(fStream, this);
		}

		public static FileInfo GetConfigFile(string filename)
		{
			var exeFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			var exeDirectoryPath = Path.GetDirectoryName(exeFilePath);
			if (exeDirectoryPath == null)
				throw new NullReferenceException("");
			
			return new FileInfo(Path.Combine(exeDirectoryPath, filename));
		}
	}
}